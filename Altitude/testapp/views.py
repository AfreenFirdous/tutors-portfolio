from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from testapp.models import Teacher
from testapp.forms import TeacherForm

# Create your views here.

def home(request):
    context = {
        'ishome': True,
    }
    return render(request, "Home.html",context=context)

def about(request):
    context = {
        'isabout' : True,
    }
    return render(request,'About.html',context=context)

def portfolio(request):
    model_obj = Teacher.objects.all().order_by('subjects','last_name')
    context={
        'obj' : model_obj,
         'isportfolio':True,

    }
    return render(request,'Portfolio.html',context=context)

def createTeacher(request):
    form = TeacherForm()
    if request.method == "POST":
        form = TeacherForm(request.POST, request.FILES)
        if form.is_valid():
            print("hello")
            form.save()
            form = TeacherForm()
    context = {
            'form': form
    }
    return render(request,'Create.html',context)


def login(request):
    context = {
        'islogin': True,
    }
    if request.method == "POST":

        if Teacher.objects.filter(first_name=request.POST['fname'], email=request.POST['email']).exists():
            return  HttpResponseRedirect("/Create/")
        else:
            context = {
                'label': 'Login Failed..',
                'islogin':True,
            }
            return render(request, 'Login.html',context=context)
    return render(request, 'Login.html',context=context)