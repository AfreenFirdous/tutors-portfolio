from django.urls import path
from . import views
from.views import createTeacher

urlpatterns = [
    path('', views.home, name='home'),
    path('Portfolio/', views.portfolio, name='portfolio'),
    path('About/', views.about, name='about'),
    path('Login/', views.login, name='login'),
    path('Create/', createTeacher),

]