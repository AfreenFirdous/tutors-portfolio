# Generated by Django 3.0.7 on 2020-06-30 06:29

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('testapp', '0003_auto_20200630_0822'),
    ]

    operations = [
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('profile', models.ImageField(default=django.utils.timezone.now, upload_to='')),
                ('email', models.EmailField(max_length=254, unique='true')),
                ('phone', models.CharField(blank=True, max_length=17)),
                ('room', models.CharField(max_length=10)),
                ('subjects', models.TextField(max_length=250)),
            ],
        ),
    ]
