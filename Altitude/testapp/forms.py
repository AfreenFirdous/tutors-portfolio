from django import forms
from testapp.models import Teacher

# class PhotoForm(forms.ModelForm):
#     class Meta:
#         model = Photo
#         fields ='__all__'

class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['first_name',
              'last_name',
              'profile',
              'email',
              'phone',
              'room',
              'subjects', ]
