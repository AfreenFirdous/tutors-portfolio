from django.db import models
from django.utils import timezone

# Create your models here.
# class Photo(models.Model):
#     name = models.CharField(max_length=20,default='admin')
#     profile = models.ImageField(upload_to='', default=timezone.now)
#
#     def __str__(self):
#         return self.name

class Teacher(models.Model):
    # phone_regex = RegexValidator(regex=r'^\+971-?\d{9,15}$',
    #                              message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

    first_name = models.CharField(max_length=50)
    last_name  = models.CharField(max_length=50)
    profile    = models.ImageField(upload_to='')
    email      = models.EmailField(max_length=254, unique="true")
    phone      = models.CharField(max_length=17, blank=True)
    room       = models.CharField(max_length=10)
    subjects   = models.CharField(max_length=60) #Change it to list of subjects